ASM=nasm
ASMFLAGS=-f elf64
LD=ld

SRC=src
OBJ=obj

PROGRAM=lab2

$(OBJ)/%.o: $(SRC)/%.asm
	mkdir -p $(OBJ)
	$(ASM) $(ASMFLAGS) -o $@ $<


$(PROGRAM): $(OBJ)/main.o $(OBJ)/lib.o $(OBJ)/dict.o 
	$(LD) -g -o $@ $+
	
clean:
	rm -rf $(OBJ) $(PROGRAM)

.PHONY: clean
