%include "src/lib.inc"

section .text

global find_word
; rdi - address of string
; rsi - pointer to dict
find_word:
    push rbx       ; rbx -> stack  , callee-saved
    mov rbx, rsi   ; rsi -> rbx    , pointer to dict

    .loop:
        lea rsi, [rbx+8]    ; addr[rbx+8] -> rsi 		, key of current pair
        call string_equals  ; string_equals(rdi, rsi) -> rax,   , check if key equals input string
        cmp rax, 1	    ; if (rax==1)
        je .found	    ;    jump .found
        mov rbx, qword[rbx] ; qword[rbx] -> rbx  		, pointer to next pair
        cmp rbx, 0	    ; if (rbx==0)
        jnz .loop           ;     break;			, check if last pair

    .not_found:		    
        xor rbx, rbx	    ; 0 -> rbx				, "not found"
	pop rbx 	    ; rbx <- stack 			, callee-saved
	ret
    .found:
        mov rax, rbx        ; rbx -> rax			, found with id of rbx
        pop rbx             ; rbx <- stack			, calle-saved
        ret		   
