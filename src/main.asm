%include "src/lib.inc"
%include "src/colon.inc"
%include "src/words.inc"
%include "src/dict.inc"

%define BUFFER_SIZE 256


section .rodata
NotFoundException_message: db "Exception! No such key in dictionary", 0xA, 0
OverflowException_message: db "Exception! Buffer overflowed (maximum is 255)", 0xA, 0

section .bss
BUFFER: resb BUFFER_SIZE

section .text

global _start
_start:
	mov rdi, BUFFER		; BUFFER -> rdi
	mov rsi, BUFFER_SIZE    ; BUFFER_SIZE -> rsi
	call read_line		; readline(rdi, rsi) -> rax
	
	cmp rax, 0		; if (rax==0)
	je .OverflowException	;  jump .OverflowException
	
	mov rdi, BUFFER		; BUFFER -> rdi
	mov rsi, DICT_POINTER	; DICT_POINTER -> rsi
	call find_word		; find_word(rdi, rsi) -> rax
		
	cmp rax, 0		; if (rax==0)
	je .NotFoundException	;  jump .NotFoundException
		
	lea rdi, [rax+8]	; addr[rax+8] -> rdi 
	push rdi		; rdi -> stack , caller-saved
	call string_length	; string_length(rdi) -> rax
	pop rdi			; rdi <-stack , caller-saved
		
	lea rdi, [rdi+rax+1]	; addr[rdi+rax+1] -> rdi
	call print_string	; print_string(rdi)
	call print_newline	; print_newline()
	
	call exit		; exit(0)
	
	.NotFoundException
		mov rdi, NotFoundException_message
		jmp .finally_error 
	.OverflowException:
		mov rdi, OverflowException_message
	.finally_error:
		call print_error ; print(error)
		call exit_error  ; exit(1)
