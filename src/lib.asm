section .text
 
 
; Принимает код возврата и завершает текущий процесс с кодом 0
global exit
exit: 
    mov rdi, 0   ; 0 -> rdi  , return 0  
    mov rax, 60  ; 60 -> rax , 'exit' identifier
    syscall
    ret 

; Принимает код возврата и завершает текущий процесс с кодом 1
global exit_error
exit_error:
    mov rdi, 1   ; 1 -> rdi  , return 1  
    mov rax, 60  ; 60 -> rax , 'exit' identifier
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; rdi - address of string
global string_length
string_length:
    xor rax, rax    ; 0 -> rax , counter
    
    .count_char:
        cmp byte[rdi+rax], 0    ; if(byte[rdi+rax]=='\0')
        je .end                 ;   break 
                                ; else {   
        inc rax                 ;   rax+=1; 
        jmp .count_char         ;   continue; 
                                ; }
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi - address of string
global print_string
print_string:
    push rdi             ; push rdi -> stack         , caller-save
    call string_length   ; string_length(rdi) -> rax , computes length of string
    pop rdi              ; pop rdi <- stack          , caller-save

    mov rdx, rax         ; rax -> rdx , amounts of bytes to write
    mov rax, 1 		 ; 1 -> rax   , 'write' syscall identifier 
    mov rsi, rdi	 ; rdi -> rsi , address of string
    mov rdi, 1		 ; 1 -> rdi   , descriptor is stdout
    syscall
    
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
; rdi - address of string
global print_error
print_error:
    push rdi             ; push rdi -> stack         , caller-save
    call string_length   ; string_length(rdi) -> rax , computes length of string
    pop rdi              ; pop rdi <- stack          , caller-save

    mov rdx, rax         ; rax -> rdx , amounts of bytes to write
    mov rax, 1 		 ; 1 -> rax   , 'write' syscall identifier 
    
    mov rsi, rdi	 ; rdi -> rsi , address of string
    mov rdi, 2		 ; 2 -> rdi   , descriptor is stderr
    syscall
    
    ret 


; Принимает код символа и выводит его в stdout
; rdi - char code
global print_char
print_char:
    xor rax, rax
    push rdi      ; push rdi to stack, now rsp holds the address to char code 
    
    mov rdx, 1    ; 1 -> rdx   , 1 byte to write
    mov rax, 1    ; 1 -> rax   , 'write' syscall identifier
    mov rsi, rsp  ; rsi -> rsp , address of cell with char code
    mov rdi, 1    ; 1 -> rdi   , descriptor is stdout
    syscall
    
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
global print_newline
print_newline:
    mov rdi, 0xA    ; 0xA -> rdi      ; as arg for print_char
    call print_char ; print_char(0xA) 
    
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; rdi - 8 bit unsigned number
global print_uint
print_uint:
    mov rax, rdi      ; rdi -> rax      , number
    
    mov r8, 10        ; 10 -> r8        , system base (10)
    mov r9, rsp       ; rsp -> r9       , callee-saved
    
    push 0            ; push 0 -> stack , /0 (end of string) 
    
    .loop:
        xor rdx, rdx       ; rdx = 0           , remainder of the division
    
        div r8	           ; rax div r8 -> rax , number without last digit
        	      	   ; rax mod r8 -> rdx , last digit
        add dl, '0'
    
        dec rsp		   ; rsp -= 1          , make space for digit
        mov byte [rsp], dl ; dl -> [rsp]       , save digit
     
        cmp rax, 0         ; if (rax == 0) 
        jne .loop	   ;	break
    
    mov rdi, rsp	; rsp -> rdi        , argument to return
    push r9		; push r9 -> stack  , caller-saved
    call print_string	; print_string(rdi) , print formed number
    pop r9		; pop r9 <- stack   , caller-saved
    
    mov rsp, r9		; r9 -> rsp	    , callee-saved
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
; rdi - 8 bit signed number
global print_int
print_int:
    cmp rdi, 0      ; if (rdi >= 0)
    jge .print      ; 	jump .print
    
    push rdi	    ; push rdi -> stack , to clear space for arg
    mov rdi, '-'    ; '-' -> rdi	, negative symbol
    call print_char ; print_char('-')   , print negative symbol
    pop rdi	    ; pop rdi <- stack  , load number
    
    neg rdi	    ; -rdi -> rdi       , to use print_uint 
.print:
    call print_uint ; print_uint(rdi)   , print number part
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
global string_equals
string_equals:
    xor rcx, rcx
    xor rax, rax
    .loop:
        mov al, [rdi+rcx] ; [rdi+rcx] -> al 	 , symbol of first string
        cmp al, [rsi+rcx] ; if (al != [rsi+rcx]) , symbol of second string
        jne .false	  ; 	jump .false	 
        inc rcx		  ; rcx += 1       	 , next iteration
        cmp al, 0	  ; if (al == 0)
        jnz .loop	  ; 	continue	 , if \0 (end of string) then break
    .true:
        mov rax, 1	  ; 1 -> rax		 , True
        ret
    .false:
        mov rax, 0	  ; 0 -> rax		 , False
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
global read_char
read_char:
    push 0	 ; 0 -> stack , buffer for char
    
    
    mov rdx, 1   ; 1 -> rdx   , 1 byte
    mov rax, 0	 ; 0 -> rax   , 'read' syscall identifier
    mov rsi, rsp ; rsp -> rsi , address to save char
    mov rdi, 0   ; 0 -> rdi   , descriptor is stdin
    syscall
    pop rax	; pop <- stack    , char code or 0 (end of stdin)
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор


; rdi - address of buffer
; rsi - size of buffer
global read_word
read_word:
    push rdi	 ; push rdi -> stack , to return later
    push r12	 ; push r12 -> stack , callee-saved
    push r13     ; push r13 -> stack , callee-saved
    push r14	 ; push r14 -> stack , callee-saved
    
    mov r12, rdi ; rdi -> r12 , address of buffer
    mov r13, rsi ; rsi -> r13 , size of buffer

    .check_for_fillers:
        call read_char		; read_char() -> rax
        cmp rax, 0x9		; if (rax == 0x9)  , if tabulation
        je .check_for_fillers	;	continue
        cmp rax, 0xA		; if (rax == 0xA)  , or end of the line 
        je .check_for_fillers	;	continue
        cmp rax, 0x20		; if (rax == 0x20) , or space then
        je .check_for_fillers	;	continue   , skip

    .loop:
        cmp rax, 0x0		; if (rax == 0x0)    , if end of the string
        je .end			;	break
        cmp rax, 0xA 		; if (rax == 0xA)    , or end of the line
        je .end			;	break
        cmp rax, 0x20		; if (rax == 0x20)   , or space then 
        je .end			;	break	     , stop

        inc r14			; r14 += 1 	     , increasing counter of chars
        cmp r14, r13		; if (r14 == r13)    , comparing current length 
        je .overflow		;  jump overflow     , of string and buffer size
        
        mov [r12+r14-1], rax	; rax -> [r12+r14-1] , saving char to buffer
	
        call read_char		; read_char() -> rax , read another symbol
        jmp .loop 		;		     , and continue the loop

    .end:
        mov byte[r12+14], 0	; 0 -> byte[r12+r14] , add \0 (of the line) to string
        mov rdx, r14		; r14 -> rdx	     , return length of word
        pop r14			; pop r13 <- stack   , callee-saved
        pop r13			; pop r14 <- stack   , callee-saved
        pop r12			; pop r12 <- stack   , callee-saved
        pop rax			; pop rax <- stack   , return address of buffer
        ret

    .overflow:
        pop r14			; pop r14 <- stack   , callee-saved
        pop r13			; pop r13 <- stack   , callee-saved
        pop r12			; pop r12 <- stack   , callee-saved
        pop rax			; pop rax <- stack   , rsp is callee-saved
        xor rax, rax		; 0 -> rax 	     , return 'fail to read' 
        ret
        
        
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер строку (до \n) из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если строка слишком большая для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

; rdi - address of buffer
; rsi - size of buffer
global read_line
read_line:
    push rdi	 ; push rdi -> stack , to return later
    push r12	 ; push r12 -> stack , callee-saved
    push r13     ; push r13 -> stack , callee-saved
    push r14	 ; push r14 -> stack , callee-saved
    
    mov r12, rdi ; rdi -> r12 , address of buffer
    mov r13, rsi ; rsi -> r13 , size of buffer

    .check_for_fillers:
        call read_char		; read_char() -> rax
        cmp rax, 0x9		; if (rax == 0x9)  , if tabulation
        je .check_for_fillers	;	continue
        cmp rax, 0xA		; if (rax == 0xA)  , or end of the line 
        je .check_for_fillers	;	continue
        cmp rax, 0x20		; if (rax == 0x20) , or space then
        je .check_for_fillers	;	continue   , skip

    .loop:
        cmp rax, 0x0		; if (rax == 0x0)    , if end of the string
        je .end			;	break
        cmp rax, 0xA 		; if (rax == 0xA)    , or end of the line
        je .end			;	break

        inc r14			; r14 += 1 	     , increasing counter of chars
        cmp r14, r13		; if (r14 == r13)    , comparing current length 
        je .overflow		;  jump overflow     , of string and buffer size
        
        mov [r12+r14-1], rax	; rax -> [r12+r14-1] , saving char to buffer
	
        call read_char		; read_char() -> rax , read another symbol
        jmp .loop 		;		     , and continue the loop

    .end:
        mov byte[r12+14], 0	; 0 -> byte[r12+r14] , add \0 (of the line) to string
        mov rdx, r14		; r14 -> rdx	     , return length of word
        pop r14			; pop r13 <- stack   , callee-saved
        pop r13			; pop r14 <- stack   , callee-saved
        pop r12			; pop r12 <- stack   , callee-saved
        pop rax			; pop rax <- stack   , return address of buffer
        ret

    .overflow:
        pop r14			; pop r14 <- stack   , callee-saved
        pop r13			; pop r13 <- stack   , callee-saved
        pop r12			; pop r12 <- stack   , callee-saved
        pop rax			; pop rax <- stack   , rsp is callee-saved
        xor rax, rax		; 0 -> rax 	     , return 'fail to read' 
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; rdi : string addres
global parse_uint
parse_uint:
    xor rax, rax	; 0 -> rax , number
    xor rcx, rcx	; 0 -> rcx , counter
    mov r10, 10		; 10 -> r10 , system base
    .loop:
        xor rdx, rdx		; 0 -> rdx 	      , current digit
        mov dl, byte[rdi+rcx]   ; byte[rdi+rcx] -> dl , current char

        cmp dl, '0'		; if (dl < '0')
        jl .end			; 	break
        cmp dl, '9'		; if (dl > '9')
        jg .end			; 	break
    
        imul rax, r10		; rax *= r10 , to add new digit
        sub rdx, '0' 		; rdx -= '0' , from ASCII	   
        add rax, rdx  		; rax += rdx , add new digit
          
        inc rcx			; rcx += 1   . increase the counter
        jmp .loop		
    .end:
        mov rdx, rcx ; rcx -> rdx , return number
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
global parse_int
parse_int:
    xor rax, rax ; 0 -> rax , number
	
    cmp byte[rdi], '-' ; if (byte[rdi] != '-')
    jne .pos	       ; 	jump .pos
.neg:
    inc rdi	       ; rdi += 1  		, to skip '-'
    call parse_uint    ; parse_uint(rdi) -> rax,rd , parse number part
    neg rax	       ; rax *= -1		, make negative it
    inc rdx	       ; rdx += 1 		, to count '-'
    ret
.pos: 
    call parse_uint    ; parse_uint(rdi) -> rax , if positive
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - buffer address
global string_copy
string_copy:
    xor rax, rax 
    
    push rdi 		; push rdi -> stack , caller-saved 
    push rsi		; push rsi -> stack , caller-saved 
    push rdx		; push rdx -> stack , caller-saved 
    call string_length
    pop rdx		; pop rdx <- stack  , caller-saved 
    pop rsi		; pop rsi <- stack  , caller-saved 
    pop rdi		; pop rdi <- stack  , caller-saved 
    
    cmp rax, rdx	; if(rdx >= rax)    , if string length
    jge .error		;   jump .error	    , then buffer size
    
    xor rcx, rcx	; 0 -> rcx 	    , buffer size
    xor rdx, rdx	; 0 -> rdx	    , buffer
.loop:
    mov dl, byte[rdi+rcx] ; byte[rdi+rcx] -> dl , copy current symbol of string
    mov byte[rsi+rcx], dl ; dl -> byte[rsi+rcx] , paste current symbol to buffer
    inc rcx		  ; rcx += 1 		, increase counter
    cmp dl, 0		  ; if (dl != 0)
    jne .loop   	  ;  continue
.done:
    ret		
.error:
    xor rax, rax	  ; 0 -> rax , return 'not enough space'
    ret

