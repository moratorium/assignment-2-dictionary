%define DICT_POINTER 0	; init pointer to our dict

%macro colon 2	; colon (key: string, label: id) 
	; exception handlers
	%ifnstr %1
		%error "string expected as first arg in macro colon"
	%endif
	%ifnid %2
		%error "id expected as second arg in macro colon"	
	%endif
	
	
	%%this:			; label refers to thi pair
	dq DICT_POINTER		; address of next pair (previous declared)
	db %1, 0		; key
	%2:			; value
						
	%define DICT_POINTER %%this ; redefine pointer, so it contains now address to next pair (previous declared)
%endmacro
